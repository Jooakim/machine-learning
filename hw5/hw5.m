clf
X = [2,2;
     4,4;
     4,0;
     0,0;
     2,0;
     0,2];
 Y = [1,1,1,-1,-1,-1];
 w = [1,1];
 b = -3;
 hold on
 plot(X(1:3,1),X(1:3,2), '+')
 plot(X(4:6,1),X(4:6,2), '*')
 
 fun = @(x,y) w(1)*x + w(2)*y + b
 syms x y
 ezplot(w(1)*x + w(2)*y + b == 0)
 
 %% Task 1.2
 
 
 
 
 
 
 %% Task 2.1
 clear all
 clc
 clf
 load('d1b.mat')
 
 K = @(x,y) dot(x,y);
 svm = svmtrain(X,Y, 'kernel_function','linear','boxconstraint',4, 'autoscale', false);
 C = svmclassify(svm,X);
 
 
 plot(X(find(C == 1),1), X(find(C == 1),2),'b+')
 hold on
 plot(X(find(C == -1),1), X(find(C == -1),2),'r*')
 ind = svm.SupportVectorIndices;
 plot(X(ind,1),X(ind,2), 'o')
 
 % Plot wrongly classified points in black
 wrongInd = find(C ~= Y);
 if ~isempty(wrongInd)
    plot(X(wrongInd,1),X(wrongInd,2), 'k*');
 end

 b = svm.Bias;
 w1 = svm.SupportVectors(:,1)'*svm.Alpha;
 w2 = svm.SupportVectors(:,2)'*svm.Alpha;
  syms x y
 ezplot(w1*x + w2*y + b == 0)
 title('')
 
disp('Bias:')
disp(b)
Margin = 2/norm([w1,w2],2)



%% Task 2.2
 clear all
 clc
 clf
 load('d2.mat')
 
 svm = svmtrain(X,Y, 'kernel_function','linear','boxconstraint',0.1, 'autoscale', false);
 C = svmclassify(svm,X);
 
 
 plot(X(find(C == 1),1), X(find(C == 1),2),'b+')
 hold on
 plot(X(find(C == -1),1), X(find(C == -1),2),'r*')
 
 % Plot wrongly classified points in black
 wrongInd = find(C ~= Y);
 if ~isempty(wrongInd)
    plot(X(wrongInd,1),X(wrongInd,2), 'k*');
 end
%%
K = 5;
indices = crossvalind('Kfold', Y, K)
%% b) linear
clc

wrong = 0;
tic
for i = 1:K
    test = find(indices == i);
    train = find(indices ~= i);
    
    svm = svmtrain(X(train,:),Y(train), 'kernel_function','linear','boxconstraint',0.1, 'autoscale', false, 'method', 'QP');
    C = svmclassify(svm,X(test,:));
    wrong = wrong + length(find(C ~= Y(test)));
end
errorLinearQP = wrong/(K*length(test))
toc


wrong = 0;
tic
for i = 1:K
    test = find(indices == i);
    train = find(indices ~= i);
    
    svm = svmtrain(X(train,:),Y(train), 'kernel%  syms  x y
%  ezplot(exp(norm(svm.SupportVectors,2))) 
_function','linear','boxconstraint',0.1, 'autoscale', false, 'method', 'SMO');
    C = svmclassify(svm,X(test,:));
    wrong = wrong + length(find(C ~= Y(test)));
end
errorLinearSMO = wrong/(K*length(test))
toc


%  
%  plot(X(find(C == 1),1), X(find(C == 1),2),'b+')
%  hold on
%  plot(X(find(C == -1),1), X(find(C == -1),2),'r*')
%  
%  % Plot wrongly classified points in black
%  wrongInd = find(C ~= Y);
%  if ~isempty(wrongInd)
%     plot(X(wrongInd,1),X(wrongInd,2), 'k*');
%  end


%% b) quad
clc

wrong = 0;
tic
for i = 1:K
    test = find(indices == i);
    train = find(indices ~= i);
    svm = svmtrain(X(train,:),Y(train), 'kernel_function','quadratic','boxconstraint',0.1, 'autoscale', false, 'method', 'QP');
    C = svmclassify(svm,X(test,:));
    wrong = wrong + length(find(C ~= Y(test)));
end
errorQuadraticQP = wrong/(K*length(test))
toc


wrong = 0;
tic
for i = 1:K
    test = find(indices == i);
    train = find(indices ~= i);
    svm = svmtrain(X(train,:),Y(train), 'kernel_function','quadratic','boxconstraint',0.1, 'autoscale', false, 'method', 'SMO');
    C = svmclassify(svm,X(test,:));
    wrong = wrong + length(find(C ~= Y(test)));
end
errorQuadraticSMO = wrong/(K*length(test))
toc
%% b) rbf
clc

wrong = 0;
tic
for i = 1:K
    test = find(indices == i);
    train = find(indices ~= i);
    svm = svmtrain(X(train,:),Y(train), 'kernel_function','rbf','boxconstraint',0.1, 'autoscale', false, 'method', 'QP');
    C = svmclassify(svm,X(test,:));
    wrong = wrong + length(find(C ~= Y(test)));
end
errorRBFQP = wrong/(K*length(test))
toc


wrong = 0;
tic
for i = 1:K
    test = find(indices == i);
    train = find(indices ~= i);
    svm = svmtrain(X(train,:),Y(train), 'kernel_function','rbf','boxconstraint',0.1, 'autoscale', false, 'method', 'SMO');
    C = svmclassify(svm,X(test,:));
    wrong = wrong + length(find(C ~= Y(test)));
end
errorRBFSMO = wrong/(K*length(test))
toc
%  syms  x y
%  ezplot(exp(norm(svm.SupportVectors,2))) 
    
svm = svmtrain(X(train,:),Y(train), 'kernel_function','rbf','boxconstraint',0.1, 'autoscale', false, 'method', 'SMO', 'showplot',true);
 
 
 %%
 
 
 w1 = svm.SupportVectors(:,1)'*svm.Alpha;
 w2 = svm.SupportVectors(:,2)'*svm.Alpha;
 
 
 K = @(x) exp(1/2*norm([(w1-x(1)),(w2-x(2))],2)^2);
 x = fminsearch(K,[1,1])
 feval(K,x)
 
 syms x y
 ezplot(exp(1/2*norm([(w1-x),(w2-y)],2)^2) == 3)
