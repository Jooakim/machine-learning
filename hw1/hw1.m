%% Task 1
clc
clf
load('dataset1.mat');
sol1_1(x);

%% Task 2
clc 
clf
clear all
hold on
load('dataset1.mat');
len = length(x(:,1));
alpha = 1;
beta = 1;
mu = sge(x);
maskMu =  repmat(mu,len,1);

fun = @(s) beta^alpha / gamma(alpha) * (s).^(-alpha-1) .* exp(-beta./(s));
funlog = @(s) -(alpha+1)*log(s) * -beta./s; 

ezplot(fun,[0,5]);

alphaPost = alpha + len;
betaPost = beta + 0.5*sum(sum(((x-maskMu)'*(x-maskMu))));

post = @(s) beta^alpha / gamma(alpha) * s.^(-alphaPost-1).*exp(-betaPost./s);
postlog = @(s) -(alphaPost+1)*log(s) * -betaPost./s; % s.^(-alphaPost-1).*exp(-betaPost./s);
ezplot(post,[0,5]);
% set(gca, 'xscale','log', 'yscale', 'log');
xlim auto
ylim auto
