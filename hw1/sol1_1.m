function [ ]=sol1_1(x)



% generate data
[mu, sigma] = sge(x);
centerX = mu(1);
centerY = mu(2);
len = length(x(:,1));

% Calculate each point's distance from mu
mask = repmat(mu,len,1);
dist = sqrt(sum((x - mask).^2,2));



hold on

f = [0,0,0];
syms xCord yCord
colors = {'b', 'r', 'k'};
style = {'-', '--', '-.'};
for k = 1:3
    radius = k*sigma;
    f(k) = length(find(dist>radius))/len;
    h = ezplot((xCord-mu(1))^2 + (yCord-mu(2))^2 == radius^2,[-1,3],[0,4]);
    set(h,'Color', colors{k}, 'LineStyle', style{k})
end
p = scatter(x(:,1),x(:,2));
set(p,'MarkerEdgeAlpha',0.8);

title([])
sF1 = num2str(f(1));
sF2 = num2str(f(2));
sF3 = num2str(f(3));
legend({sF1,sF2,sF3});




end