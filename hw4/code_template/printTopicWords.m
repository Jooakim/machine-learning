function printTopicWords(beta, vocabulary, nbrOfWords)


    for topic = 1:length(beta(:,1))
        [sortedBeta,sortingIndices] = sort(beta(topic,:),'descend');
        ind = sortingIndices(1:nbrOfWords);
        fprintf('Topic %d \n', topic);
        disp(vocabulary(ind));    
    end


end

