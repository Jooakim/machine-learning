clear all
clc

mu = [1,1];
Sigma = [1,-0.5; -0.5 1];
x1 = -2:.2:4; x2 = -2:.2:4;
[X1,X2] = meshgrid(x1,x2);
F = mvnpdf([X1(:) X2(:)],mu,Sigma);
F = reshape(F,length(x2),length(x1));
surf(x1,x2,F);
xlabel('x1'); ylabel('x2'); zlabel('Probability Density');

%%

p=[0.2,0.3, 0.5, 0];

for i = 1:1000
   AA(i)=find(mnrnd(1,p)==1);
end
hist(AA)

%%
s = 0;
for i = 1:100
    s = s + sum(wordcount{i}.cnt);
end
s