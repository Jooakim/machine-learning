function extractTopWords(wordcount, vocabulary, doc)

    N = 10;

    [~, sortingIndices] = sort(wordcount{doc}.cnt,'descend');
    ind = sortingIndices(1:N);
    fprintf('Top words \n');
    disp(vocabulary(wordcount{doc}.id(ind)));

end