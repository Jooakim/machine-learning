%% Task 2.1
clc

load('dataset2.mat')

% Variables
nbrOfFolds = 5;
features = x;
class = y;
len = length(class(:,1))/2;

% Initialization
firstError = 0;
secondError = 0;


indices = crossvalind('Kfold', len, nbrOfFolds);

for run = 1:nbrOfFolds
    test = find(indices == run);
    train = find(indices ~= run);
    
    
    c1 = find(class == 1);
    c2 = find(class == -1);
    [mu1, std1]= sge(features(c1(train),:));
    var1 = std1^2;
    [mu2, std2] = sge(features(c2(train),:));
    var2 = std2^2;
    
    % Classify using sph_bayes
    for i = 1:length(test)
        [~ , ~, newclass1(i)] = sph_bayes(features(c1(test(i)),:) ...
            , mu1, mu2, var1, var2);
        [~ , ~, newclass1(length(test)+i)] = sph_bayes(features(c2(test(i)),:) ...
            , mu1, mu2, var1, var2);
    end
    % Classify using new_classifier
    newclass2 = new_classifier([features(c1(test),:); features(c2(test),:)], mu1, mu2);
    
    len2 = length(test);
    % Calculate the errors of sph_bayes
    firstError = firstError + length(find(newclass1(1:len2) == -1)) + ...
        + length(find(newclass1(len2+1:end) == 1));
    
    % Calculate the errors of new_classifier
    secondError = secondError + length(find(newclass2(1:len2) == -1)) + ...
        + length(find(newclass2(len2+1:end) == 1));
end

firstError = firstError/(nbrOfFolds*length(newclass1))
secondError = secondError/(nbrOfFolds*length(newclass2))




%% Task 2.2
clear
clc


load('digits.mat')

class5 = data(:,:,5)';
class8 = data(:,:,8)';

nbrOfFolds = 5;

len = length(class5(:,1));
fiveError = 0;
eigthError = 0;

indices = crossvalind('Kfold', len, nbrOfFolds);

for i = 1:nbrOfFolds
    
    
    
    train = find(indices ~= i);
    test = find(indices == i);
    
    mu1 = mean(class5(train,:));
    mu2 = mean(class8(train,:));
    
    classification = new_classifier([class5(test,:); class8(test,:)], mu1, mu2);
    len2 = length(classification)/2;
    fiveError = fiveError + length(find(classification(1:len2) == -1))/len2;
    eigthError = eigthError + length(find(classification(len2+1:end) == 1))/len2;
end

fiveError = fiveError/nbrOfFolds;
eigthError = eigthError/nbrOfFolds;
totalError = (fiveError + eigthError)/2;



%% Alternative way, aka b)
clc

load('digits.mat')
nbrOfFolds = 5;
newFeatVec5 = zeros(length(class5),32);
newFeatVec8 = zeros(length(class8),32);

fiveErrorAlt = 0;
eigthErrorAlt = 0;

len = length(newFeatVec5(:,1));
indices = crossvalind('Kfold', len, nbrOfFolds);

for observation = 1:length(class5(:,1))
    y5 = reshape(data(:, observation, 5) , 16, 16); % 16x16 image
    y8 = reshape(data(:, observation, 8) , 16, 16); % 16x16 image
    
    for i = 1:16
        % rows
        z = y5(i, :)/255;
        newFeatVec5(observation, i) = var(z);
        z = y8(i, :)/255;
        newFeatVec8(observation, i) = var(z);
        
        % columns
        z = y5(:, i)/255;
        newFeatVec5(observation, 16+i) = var(z);
        z = y8(:, i)/255;
        newFeatVec8(observation, 16+i) = var(z);
    end
    
end


for k = 1:nbrOfFolds
    
    
    
    
    train = find(indices ~= k);
    test = find(indices == k);
    
    mu11 = mean(newFeatVec5(train,:));
    mu22 = mean(newFeatVec8(train,:));
    
    classification2 = new_classifier([newFeatVec5(test,:); newFeatVec8(test,:)], mu11, mu22);
    
    len = length(classification2)/2;
    fiveErrorAlt = fiveErrorAlt + length(find(classification2(1:len) == -1))/len;
    eigthErrorAlt = eigthErrorAlt + length(find(classification2(len+1:end) == 1))/len;
    
end
% Calculate the average error
fiveErrorAlt = fiveErrorAlt/nbrOfFolds;
eigthErrorAlt = eigthErrorAlt/nbrOfFolds;
totalError2 = (fiveErrorAlt + eigthErrorAlt)/2;


y = reshape(data(:, 79, 5) , 16, 16); % 16x16 image
imagesc(y); % show image



%% Optional test on pixel vector
clear
clc


load('digits.mat')

class5 = data(:,:,5)';
class8 = data(:,:,8)';

nbrOfFolds = 5;

len = length(class5(:,1));
fiveError = 0;
eigthError = 0;

min = 100;

% % Preprocessing, "Binaryfication"
% th = 0;
% class5 = data(:,:,5)';
% class8 = data(:,:,8)';
% class5(class5<=th) = 0;
% class8(class8<=th) = 0;
% class5(class5>th) = 1;
% class8(class8>th) = 1;

% % Preprocessing, applying filter, blurring/noise-removal
% for i = 1:length(class5)
%     m = reshape(class5(i,:),16,16);
%     J = medfilt2(m,[2,2]);
% %     J = wiener2(m,[2,2]);
%
%     class5(i,:) = reshape(J, 1, 256);
%
%     m = reshape(class8(i,:),16,16);
%     J = medfilt2(m,[2,2]);
% %     J = wiener2(m,[2,2]);
%     class8(i,:) = reshape(J, 1, 256);
% end



indices = crossvalind('Kfold', len, nbrOfFolds);

for i = 1:nbrOfFolds
    
    
    
    train = find(indices ~= i);
    test = find(indices == i);
    
    mu1 = mean(class5(train,:));
    mu2 = mean(class8(train,:));
    
    classification = new_classifier([class5(test,:); class8(test,:)], mu1, mu2);
    len2 = length(classification)/2;
    fiveError = fiveError + length(find(classification(1:len2) == -1))/len2;
    eigthError = eigthError + length(find(classification(len2+1:end) == 1))/len2;
end

fiveError = fiveError/nbrOfFolds;
eigthError = eigthError/nbrOfFolds;
totalError = (fiveError + eigthError)/2;


%% Optional test on variance vector
clc

load('digits.mat')

class5 = data(:,:,5)';
class8 = data(:,:,8)';



% % Preprocessing, applying filter
% for i = 1:length(class5)
%     m = reshape(class5(i,:),16,16);
%     J = wiener2(m,[2,2]);
% %         J = medfilt2(m,[2,2]);
%     class5(i,:) = reshape(J, 1, 256);
% 
%     m = reshape(class8(i,:),16,16);
%     J = wiener2(m,[2,2]);
% %         J = medfilt2(m,[2,2]);
%     class8(i,:) = reshape(J, 1, 256);
% end

% Preprocessing, "high-pass filter"
th = 0;
class5 = data(:,:,5)';
class8 = data(:,:,8)';
class5(class5<=th) = 0;
class8(class8<=th) = 0;
class5(class5>th) = 1;
class8(class8>th) = 1;



nbrOfFolds = 5;
newFeatVec5 = zeros(length(class5),32);
newFeatVec8 = zeros(length(class8),32);

fiveErrorAlt = 0;
eigthErrorAlt = 0;

len = length(newFeatVec5(:,1));
indices = crossvalind('Kfold', len, nbrOfFolds);




for observation = 1:length(class5(:,1))
    y5 = reshape(class5(observation,:) , 16, 16); % 16x16 image
    y8 = reshape(class8(observation,:) , 16, 16); % 16x16 image
    
    
    for i = 1:16
        % rows
        z = y5(i, :)/255;
        newFeatVec5(observation, i) = var(z);
        z = y8(i, :)/255;
        newFeatVec8(observation, i) = var(z);
        
        % columns
        z = y5(:, i)/255;
        newFeatVec5(observation, 16+i) = var(z);
        z = y8(:, i)/255;
        newFeatVec8(observation, 16+i) = var(z);
    end
    
end




for k = 1:nbrOfFolds
    
    train = find(indices ~= k);
    test = find(indices == k);
    
    mu11 = mean(newFeatVec5(train,:));
    mu22 = mean(newFeatVec8(train,:));
    
    classification2 = new_classifier([newFeatVec5(test,:); newFeatVec8(test,:)], mu11, mu22);
    
    len = length(classification2)/2;
    fiveErrorAlt = fiveErrorAlt + length(find(classification2(1:len) == -1))/len;
    eigthErrorAlt = eigthErrorAlt + length(find(classification2(len+1:end) == 1))/len;
    
end
% Calculate the average error
fiveErrorAlt = fiveErrorAlt/nbrOfFolds;
eigthErrorAlt = eigthErrorAlt/nbrOfFolds;
totalError2 = (fiveErrorAlt + eigthErrorAlt)/2;


y = reshape(data(:, 79, 5) , 16, 16); % 16x16 image
imagesc(y); % show image


