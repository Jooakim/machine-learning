clear all
clc
clf

load('hw6_p1a.mat')
data = X;

K = 2;
dim = 2;
observations = length(data(:,1));
ZNK = zeros(observations, K);
ZNK_old = zeros(observations, K)+1;
iter = 0;
% Random initialization of mu
mu_k = rand(K,dim);
while ~isequal(ZNK_old, ZNK)
    iter = iter + 1;
    % Assign datapoints to closest mu
    classes = FindCluster(data, mu_k, 'linear');
    
    ZNK_old = ZNK;
    ZNK = zeros(observations, K);
    for i = 1:observations
        ZNK(i,classes(i)) = 1;
    end
    
    for k = 1:K
        mu_k(k,:) = ZNK(:,k)'*data/sum(ZNK(:,k));
    end
    
    if iter == 2
        classes_iter2 =  classes;
    end
end
hold on
gscatter(data(:,1),data(:,2),classes,'br','x.')
indexOfChange = find(classes ~= classes_iter2);
scatter(data(indexOfChange,1),data(indexOfChange,2), 'ko')
legend('1','2','Changed')

%% Task 1c

clear all
clc
clf

load('hw6_p1b.mat')
data = X;

K = 2;
dim = 2;
observations = length(data(:,1));

ZNK = zeros(observations, K);
ZNK_old = zeros(observations, K)+1;
iter = 0;

% Initialize random assignements
for i = 1:observations
    ZNK(i,randi(K)) = 1;
end

% Classify with RBF-Kernel

while ~isequal(ZNK_old, ZNK)
    iter = iter + 1;
    % Assign datapoints to closest mu
    classes = FindCluster(data, 0, 'rbf', ZNK);
    
    ZNK_old = ZNK;
    ZNK = zeros(observations, K);
    for i = 1:observations
        ZNK(i,classes(i)) = 1;
    end
end
gscatter(data(:,1),data(:,2),classes,'br','x.')


% Classify according to linear k-means
figure

% Reset
ZNK = zeros(observations, K);
ZNK_old = zeros(observations, K)+1;

% Random initialization of mu
mu_k = rand(K,dim);
while ~isequal(ZNK_old, ZNK)
    iter = iter + 1;
    % Assign datapoints to closest mu
    classes = FindCluster(data, mu_k, 'linear');
    
    ZNK_old = ZNK;
    ZNK = zeros(observations, K);
    for i = 1:observations
        ZNK(i,classes(i)) = 1;
    end
    
    for k = 1:K
        mu_k(k,:) = ZNK(:,k)'*data/sum(ZNK(:,k));
    end
end
gscatter(data(:,1),data(:,2),classes,'br','x.')

%% Task 1.2a
clear all
clc
clf

% Initialize
load('medium_100_10k.mat')
K = 10;

[~,~,~,D] = kmeans(wordembeddings,K);
topWords = PrintClosestWords(D,vocab,10);

% Outcommented since class was not written by us.
% matrix2latex(topWords','test.txt','columnLabels',[1,2,3,4,5,6,7,8,9,10])

%% Task 1.2b
clear all
clc
clf

% Initialize
load('medium_100_10k.mat')
K = 10;
nbrOfRuns = 10;
f = zeros(nbrOfRuns,1);

% Localize index of calvalry
for index = 1:length(vocab)
    if strcmp(vocab{index},'cavalry')
        break
    end
end

for run = 1:nbrOfRuns
    
    % Classify and get indices of all points
    % in same class as cavalry
    [idx1] = kmeans(wordembeddings,K, 'Replicates', 1);
    cluster = idx1(index);
    clusterIndices = find(idx1 == cluster);
    N0 = sum(1:length(clusterIndices)-1);
    
    % Classify a second time and calculate N1
    [idx2] = kmeans(wordembeddings,K, 'Replicates', 1);
    N1 = 0;
    for k = 1:K
        tempWords = find(idx2(clusterIndices) == k);
        N1 = N1 + sum(1:length(tempWords)-1);
    end
    
    f(run) = N1/N0;
    
end
% Display the average f-value
disp(sum(f)/nbrOfRuns)

%% Task 1.2c
clear all
clf
clc

% Initialize
load('medium_100_10k.mat')
nbrOfSamples = 1000;
K = 10;

samples = randsample(length(vocab),nbrOfSamples);

% Classify and visualize
[labels] = kmeans(wordembeddings(samples,:),K,'Replicates',1);
downsamples = tsne(wordembeddings(samples,:));
gscatter(downsamples(:,1),downsamples(:,2),labels)














