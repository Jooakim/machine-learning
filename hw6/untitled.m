
clear all
clc
clf

load('hw6_p1b.mat')
data = X;

K = 2;
dim = 2;
observations = length(data(:,1));

ZNK = zeros(observations, K);
ZNK_old = zeros(observations, K)+1;
iter = 0;

% Initialize random assignements
for i = 1:observations
    ZNK(i,randi(K)) = 1;
end

while ~isequal(ZNK_old, ZNK)
    iter = iter + 1;
    % Assign datapoints to closest mu
    classes = FindCluster(data, 0, 'rbf', ZNK);
    
    ZNK_old = ZNK;
    ZNK = zeros(observations, K);
    for i = 1:observations
        ZNK(i,classes(i)) = 1;
    end
    
    if iter == 2
        classes_iter2 =  classes;
    end
end
hold on
gscatter(data(:,1),data(:,2),classes,'br','x.')
indexOfChange = find(classes ~= classes_iter2);
scatter(data(indexOfChange,1),data(indexOfChange,2), 'ko')
disp('DONE')

%%
for i = 1:10000
    if strcmp('to',vocab{i})
        i
    end
end