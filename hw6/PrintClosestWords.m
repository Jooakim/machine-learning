function topWords = PrintClosestWords(matrix, vocabulary, nbrOfWords)

    topWords = {};
    for cluster = 1:length(matrix(1,:))
        [~, sortingIndices] = sort(matrix(:,cluster),'ascend');
        ind = sortingIndices(1:nbrOfWords);
        fprintf('Topic %d \n', cluster);
        for i = 1:nbrOfWords
            disp(vocabulary{ind(i)});   
            topWords{cluster,i} = vocabulary{ind(i)};
        end
        fprintf('\n');
    end


end

