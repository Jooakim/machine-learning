clc
clf
format long

load('dataset');
X = dataset0;
cX = cov(X);
rX = corrcov(cX);

Y = zeros(size(X));
for i = 1:length(X(1,:))
    Y(:,i) = rand*X(:,i);
end

cY = cov(Y);
rY = corrcov(cY);

mesh(rX)
figure
mesh(rY)

scatter(Y(:,4), Y(:,12))

